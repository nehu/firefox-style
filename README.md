# firefox-style

A custom style to make Firefox and derivatives look better. Includes transparency if supported it.

## How to install

### Install userChrome.css

Navigate to your profile directory. It's probably under `~/.firefox` (or `~/.librewolf`, ... respectively) for native installations, or under `~/.var/app/org.mozilla.firefox/.firefox` (or `~/.var/app/io.gitlab.librewolf-community/.librewolf`, ...) if installed with Flatpak.

In the profile directory, clone this repository under the name `chrome`.

Example:

```sh
cd .var/app/io.gitlab.librewolf-community/.librewolf/9vhu4m49.default-default
git clone https://git.disroot.org/nehu/firefox-style.git chrome
```

### Enable customization option in Firefox

-	In your browser, navigate to `about:config`.
-	Search for `userprof`.
-	Doubleclick `toolkit.legacyUserProfileCustomizations.stylesheets` to set the value to `true`.
-	Restart your browser.

## Credits

_Credits to [userchrome.org](https://www.userchrome.org/how-create-userchrome-css.html) for the many resources around userChrome.css._
